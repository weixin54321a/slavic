const getters = {
  menus: state => state.menu.menus,
  resources: state => state.resource.resources
}
export default getters