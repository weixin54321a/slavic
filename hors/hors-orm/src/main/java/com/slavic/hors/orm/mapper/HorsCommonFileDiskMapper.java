package com.slavic.hors.orm.mapper;

import com.slavic.hors.orm.entity.HorsCommonFileDisk;
import com.slavic.hors.orm.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * (HorsCommonFileDisk)表数据库访问层
 */
@Mapper
public interface HorsCommonFileDiskMapper extends BaseMapper<HorsCommonFileDisk, Long>{

}