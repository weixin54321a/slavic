package com.slavic.hors.orm.mapper;

import com.slavic.hors.orm.entity.HorsPortalLoginRecord;
import com.slavic.hors.orm.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * (HorsPortalLoginRecord)表数据库访问层
 */
@Mapper
public interface HorsPortalLoginRecordMapper extends BaseMapper<HorsPortalLoginRecord, Long>{

}