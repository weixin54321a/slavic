package com.slavic.vesna.leet.leet;

import java.util.Arrays;

public class Leet5476 {

    public static void main(String[] args) {
        System.out.println(new Leet5476().getWinner(new int[]{3,2,1}, 10));
    }

    public int getWinner(int[] arr, int k) {
        k = Math.min(arr.length,k);
        int count = 0, winner = 0;
        while (count < k) {
            if (arr[0] > arr[1]) {
                count++;
                winner = arr[0];
            } else {
                winner = arr[1];
                count = 1;
            }
            int index = arr[0] > arr[1] ? 1 : 0;
            int temp = arr[index];
            System.arraycopy(arr, index + 1, arr, index, arr.length - 1 - index);
            arr[arr.length - 1] = temp;
        }

        return winner;
    }
}
