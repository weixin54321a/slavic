package com.slavic.vesna.leet.leet;

public class Leet5475 {

    public static void main(String[] args) {
        System.out.println(new Leet5475().countGoodTriplets(new int[]{1, 1, 2, 2, 3}, 0, 0, 1));
    }

    public int countGoodTriplets(int[] arr, int a, int b, int c) {

        int count = 0;
        if (null == arr || arr.length < 2) {
            return count;
        }
        for (int i = 0; i < arr.length - 2; i++) {

            for (int j = i + 1; j < arr.length - 1; j++) {

                if (Math.abs(arr[i] - arr[j]) <= a) {
                    for (int k = j + 1; k < arr.length; k++) {

                        if (Math.abs(arr[j] - arr[k]) <= b && Math.abs(arr[i] - arr[k]) <= c) {
                            count++;
                        }

                    }
                }
            }

        }

        return count;
    }

}
